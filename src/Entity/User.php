<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User extends BaseUser {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    public $id;

    public function getId() {
        return $this->id;
    }

    /**
     * @ORM\GeneratedValue()
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    public $firma;

    /**
     * @ORM\GeneratedValue()
     * @ORM\Column(type="string")
     */
    public $name;

    /**
     * @ORM\GeneratedValue()
     * @ORM\Column(type="string")
     */
    public $vorname;

    /**
     * @ORM\GeneratedValue()
     * @@ORM\Column(type="string", length=64, nullable=true)
     */
   public $adresse;

    /**
     * @ORM\GeneratedValue()
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    public $plz;

    /**
     * @ORM\GeneratedValue()
     *@ORM\Column(type="string", length=64, nullable=true)
     */
    public $ort;

    /**
     * @ORM\GeneratedValue()
     * @ORM\Column(type="string")
     */
   public $land;
   
  
   public $email;
           function getFirma() {
        return $this->firma;
    }

    function getName() {
        return $this->name;
    }

    function getVorname() {
        return $this->vorname;
    }

    function getAdresse() {
        return $this->adresse;
    }

    function getPlz() {
        return $this->plz;
    }

    function getOrt() {
        return $this->ort;
    }

    function getLand() {
        return $this->land;
    }

    function setFirma($firma) {
        $this->firma = $firma;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setVorname($vorname) {
        $this->vorname = $vorname;
    }

    function setAdresse($adresse) {
        $this->adresse = $adresse;
    }

    function setPlz($plz) {
        $this->plz = $plz;
    }

    function setOrt($ort) {
        $this->ort = $ort;
    }

    function setLand($land) {
        $this->land = $land;
    }


}
