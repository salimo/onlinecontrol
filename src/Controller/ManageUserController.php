<?php

// src/Controller/LuckyController.php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\User as Users;

class ManageUserController extends \Symfony\Bundle\FrameworkBundle\Controller\Controller {

    /**
     * 
     * @Route( "/manageuser/createuser" , name ="createUser")
     */
    public function index(Request $request) {

        $message = [];




        $users = new Users();

        $form = $this->createFormBuilder($users)
                ->add('name', TextType::class, array('attr' => array('class' => 'form-control')))
                ->add('vorname', TextType::class, array('attr' => array('class' => 'form-control')))
                ->add('username', TextType::class, array('attr' => array('class' => 'form-control')))
                ->add('email', \Symfony\Component\Form\Extension\Core\Type\EmailType::class, array('attr' => array('class' => 'form-control')))
                ->add('firma', TextType::class, array(
                    'required' => false,
                    'attr' => array('class' => 'form-control'),
                ))
                ->add('land', TextType::class, array(
                    'required' => false,
                    'attr' => array('class' => 'form-control'),
                ))
                ->add('ort', TextType::class, array(
                    'required' => false,
                    'attr' => array('class' => 'form-control'),
                ))
                ->add('adresse', TextType::class, array(
                    'required' => false,
                    'attr' => array('class' => 'form-control'),
                ))
                ->add('plz', TextType::class, array(
                    'required' => false,
                    'attr' => array('class' => 'form-control'),
                ))

                ->getForm();

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {





            $userManager = $this->get('fos_user.user_manager');

            // Or you can use the doctrine entity manager if you want instead the fosuser manager
            // to find 
            //$em = $this->getDoctrine()->getManager();
            //$usersRepository = $em->getRepository("mybundleuserBundle:User");
            // or use directly the namespace and the name of the class 
            // $usersRepository = $em->getRepository("mybundle\userBundle\Entity\User");
            //$email_exist = $usersRepository->findOneBy(array('email' => $email));

            $email_exist = $userManager->findUserByEmail($users->getEmail());
            $username_exist = $userManager->findUserByUsername($users->getUsername());

            // Check if the user exists to prevent Integrity constraint violation error in the insertion
            if ($email_exist || $username_exist) {
                $message["errormessage"] = 'User name or Email are already taken ';
                $message["added"] = 0;
                return $this->render('admin/createuser.html.twig', array('form' => $form->createView(), 'message' => $message));
            }
            $password = $this->randw(6);
            try {
                $users->setEnabled(true);
                $users->setPlainPassword($password);
                $userManager->updateUser($users);
                $message["added"] = 1;
                $this->sendEmail($users, $password);
            } catch (\Doctrine\DBAL\Exception\UniqueConstraintViolationException $ex) {
                $message["added"] = 0;
                $message["errormessage"] = 'User könnte nicht hinzegefügt werden';
            }
        }
        return $this->render('admin/createuser.html.twig', array('form' => $form->createView(), 'message' => $message));
    }

    function sendEmail($users, $password) {

        $transport = (new \Swift_SmtpTransport('smtp.onlinecontrolserver.net', 25))
                ->setUsername('noreply@onlinecontrolserver.net')
                ->setPassword('Servus2018')
        ;

        /*
          You could alternatively use a different transport such as Sendmail:

          // Sendmail
          $transport = new Swift_SendmailTransport('/usr/sbin/sendmail -bs');
         */

// Create the Mailer using your created Transport
        $mailer = new \Swift_Mailer($transport);


// Create a message
        $message = (new \Swift_Message('Email confirmation'))
                ->setFrom('noreply@onlinecontrolserver.net')
                ->setTo($users->getEmail())
                ->setSubject('Your Onlinecontrol Account is activated')
                ->setBody('Your Username: ' . $users->getUsername() . ' and ' . 'your passwort is ' . $password.'<br> login at: https://s2.onlinecontrolserver.net')
        ;
        $mailer->send($message);
    }

    function randw($length = 4) {
        return substr(str_shuffle("qwertyuiopasdfghjklzxcvbnm"), 0, $length);
    }
 /**
     * 
     * @Route( "/manageuser/manageuser" , name ="manageuser")
     */
    public function Listuser(Request $request)
    {   
  $userManager = $this->get('fos_user.user_manager');
 $users = $userManager->findUsers();

   return $this->render('admin/manageuser.html.twig',array ('users'=>$users));

   
}

/**
     * 
     * @Route( "/manageuser/deleteuser/{userid}" , name ="deleteuser")
     */
    public function DeleteUser(Request $request,$userid)
    {   
        
    $entityManager = $this->getDoctrine()->getManager();
    $user = $entityManager->getRepository(\App\Entity\User::class)->find($userid);
      if (!$user) {
        throw $this->createNotFoundException(
            'No user found for id '.$id
        );
    }
  $entityManager->remove($user);
  $entityManager->flush();
    return $this->redirect($this->generateUrl('manageuser', array('message' => 'Used has been deleted succesfuly')));

   
}


/**
     * 
     * @Route( "/manageuser/getusterByid/{userid}" , name ="updateuser")
     */
    public function GetUserbyId(Request $request,$userid)
    {   
       
    $entityManager = $this->getDoctrine()->getManager();
    $user = $entityManager->getRepository(\App\Entity\User::class)->find($userid);
      if (!$user) {
        throw $this->createNotFoundException(
            'No user found for id '.$id
        );
    }
$serializer = $this->container->get('serializer');
$userjson = $serializer->serialize($user, 'json');
 $response= new JsonResponse($userjson);
return $response;
}

/**
     * 
     * @Route( "/updateuser" , name ="updateuserInfo")
     */
    public function UpdateUser(Request $request)
    {    $entityManager = $this->getDoctrine()->getManager();
      if ($request->isXMLHttpRequest()) {
          
        $serializer = $this->container->get('serializer');
        $_POST['user']['id']=(int)$_POST['user']['id'];
        
        $newuser = $serializer->deserialize(json_encode($_POST['user']),'App\Entity\User', 'json');
       
        $olduserr = $entityManager->getRepository(\App\Entity\User::class)->find($newuser->getId());
        
        
       $olduserr->setName($newuser->getName());
       $olduserr->setUsername($newuser->getUsername());
       $olduserr->setVorname($newuser->getVorname()); 
       $olduserr->setEmail($newuser->email); 
       $olduserr->setLand($newuser->getLand());
       $olduserr->setOrt($newuser->getOrt()); 
       $olduserr->setPlz($newuser->getPlz()); 
    
       
       $entityManager->flush();
      $userjson = $serializer->serialize($newuser, 'json');
       return new JsonResponse(array('data' => json_encode($olduserr)));
    }

    return new Response('This is not ajax!', 400);

}

}
