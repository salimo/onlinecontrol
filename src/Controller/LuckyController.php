<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller;

class LuckyController extends Controller\AbstractController
{
    
    /**
     * 
     * @Route("/")
     */
    public function index()
    {
        
if( $this->getUser()  ){
  
     return $this->render('index.html.twig');
}

else
    return $this->redirect($this->generateUrl('fos_user_security_login'));
       
    }
    
   
}